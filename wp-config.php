<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eterma');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z](Z}BC9gsmCZ:|]1SBc.Z2JiWeuK)YmGz:G9Rg~F[s3N5k8tJlZ+}YkqQ}[U:nV');
define('SECURE_AUTH_KEY',  'rGZ?$@8o{Fhv!8-AF?v3QALzyNUk#e$AjdVjN+I+n-XrYf[Z)I,;|,2h95zl+E((');
define('LOGGED_IN_KEY',    ';N?M}h31=bIrk/QU+}$T9XAlIsXAL+U_lpeV~DuFYfRM@1W+:(i9@+F`hKYn6<U/');
define('NONCE_KEY',        'M&0+6O_GZKSRW_u|>&Q!VBG55JwqIWoS7.Js D_mqs4w~z[.;6g*O9y-.B(|!kM|');
define('AUTH_SALT',        'T~zKm0T@^-pM,1p9&txqsNJ(=>#z]ym$495(9u |E?AB:Un-(N:5$WxKG[Ps$tp ');
define('SECURE_AUTH_SALT', ':Y_eCy5#E[r4$Ci_Fm9qt1,g/vtczd?nfhU?zz}=[KATT{Fyh%=%5`IUz&<h}F@e');
define('LOGGED_IN_SALT',   '[E8R/tyW87he+-eI*pD3# j?0FPZaF1-H(K#dB`BKU7q iyU~w0:^HQ:.5<C-U%I');
define('NONCE_SALT',       'L4,~<t`%Iwb>Gt%y#RZ`,,7Gd P5ofus/xnY}.z9bv+5NBt+|7v<$=_u_Vd0O|<[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
