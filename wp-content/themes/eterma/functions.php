<?php

/**
 * Defined constants
 */
define( 'TEMPLATE_DIR_PATH', get_template_directory() );
define( 'TEMPLATE_DIR_URI', get_template_directory_uri() );


/**
 * Register styles and JS
 */

	wp_enqueue_style( 'bootstrap', TEMPLATE_DIR_URI . '/assets/style/bootstrap.min.css', array() );
	wp_enqueue_style( 'font-awesome', TEMPLATE_DIR_URI . '/assets/style/font-awesome.min.css', array() );
	wp_enqueue_style( 'animate', TEMPLATE_DIR_URI . '/assets/style/animate.css', array() );
	wp_enqueue_style( 'normalize', TEMPLATE_DIR_URI . '/assets/style/normalize.css', array() );
	wp_enqueue_style( 'main', TEMPLATE_DIR_URI . '/assets/style/scss/main.css', array() );
	wp_enqueue_style( 'header', TEMPLATE_DIR_URI . '/assets/style/scss/header.css', array() );
	wp_enqueue_style( 'footer', TEMPLATE_DIR_URI . '/assets/style/scss/footer.css', array() );
	wp_enqueue_style( 'content', TEMPLATE_DIR_URI . '/assets/style/scss/content.css', array() );
	wp_enqueue_style( 'sidebar', TEMPLATE_DIR_URI . '/assets/style/scss/sidebar.css', array() );


	wp_enqueue_script( 'jQuery', TEMPLATE_DIR_URI . '/assets/js/jquery-2.2.2.min.js', array() );
	wp_enqueue_script( 'script', TEMPLATE_DIR_URI . '/assets/js/script.js', array() );



/**
 * Register default menus
 */
function register_my_menu() {
	register_nav_menus( array(
		'primary' => __( 'Header Menu', 'theme' ),
		'footer'  => __( 'Footer Menu', 'theme' )
	) );
}
add_action( 'after_setup_theme', 'register_my_menu' );

// Add custom fields to pages for sidebar
add_action( 'cmb2_admin_init', 'cmb2_sidebar' );

function cmb2_sidebar() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_eterma_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'sidebar_metabox',
		'title'         => __( 'Sidebar', 'cmb2' ),
		'object_types'  => array( 'page', ), // Post type
		'show_on'      => array( 'key' => 'id', 'value' => array( 57 ) ),
		'context'       => 'normal', //normal, advanced or side
		'priority'      => 'core', // high,medium,low,core
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );


	$group_field_id = $cmb->add_field( array(
		'id'          => $prefix . 'sidebar_group',
		'type'        => 'group',
		'description' => __( 'Generate sidebar', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
			'group_title'   => __( 'Box {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'    => __( 'Add Another Box', 'cmb2' ),
			'remove_button' => __( 'Remove Box', 'cmb2' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );


	// Sidebar title field
	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Sidebar box title',
		'desc'    => 'Write title of box here',
		'default' => '',
		'id'      => 'sidebar_title',
		'type'    => 'text',
	) );

	// Sibear image field
	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Sidebar box image', 'cmb2' ),
		'desc' => __( 'Select image', 'cmb2' ),
		'id'   => 'sidebar_image',
		'type' => 'file',
//		'options' => array(
//			'url' => false, // Hide the text input for the url
//			'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
//		),
	) );

	// Sidebar text field
	$cmb->add_group_field( $group_field_id, array(
		'name' => 'Sidebar box text',
		'desc' => 'Write sidebar box text here',
		'default' => '',
		'id' => 'sidebar_text',
		'type' => 'textarea_small'
	) );


}

//Remove text editor from sidebar page

add_action( 'admin_head', 'hide_editor' );
function hide_editor() {
	// Get the Post ID.
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if( !isset( $post_id ) ) return;

	// Hide the editor on the page titled 'Sidebar'
	$homepgname = get_the_title($post_id);
	if($homepgname == 'Sidebar'){
		remove_post_type_support('page', 'editor');
	}
}