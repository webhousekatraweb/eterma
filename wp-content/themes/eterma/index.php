<?php
/**
 * Created by PhpStorm.
 * User: Tomasz
 * Date: 13/03/16
 * Time: 10:38
 */

get_header(); ?>

	<div class="container p-x-0">
		<div class="col-md-12">
			<div class="left col-md-9 text-justify">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : ?>
						<h2><?php the_title(); ?></h2>
				<?php
						the_post();
						the_content();
					endwhile;
				endif;
				?>
			</div>

			<?php get_sidebar(); ?>

		</div>
	</div>

<?php get_footer(); ?>