<?php
/**
 * Created by PhpStorm.
 * User: Tomasz
 * Date: 13/03/16
 * Time: 12:07
 */
?>

<div class="sidebar col-md-3">
	<div class="box">
		<?php
			$sidebarGroup  = get_post_meta( 57, '_eterma_sidebar_group', true );

			if(!empty($sidebarGroup)) {
				foreach ($sidebarGroup as $box) {
					;?>
					<div class="box-item">
					<?php
					if (isset($box['sidebar_title'])) {
						?>
						<div class="title">
							<h3><?php echo esc_html($box['sidebar_title']); ?></h3>
						</div>
						<?php
					}

					if (isset($box['sidebar_image'])) {
						?>
						<div class="image">
							<img src="<?php echo $box['sidebar_image']; ?>" alt="">
						</div>
						<?php
					}

					if (isset($box['sidebar_text'])) {
						?>
						<div class="text">
							<?php echo $box['sidebar_text']; ?>
						</div>
						<?php
					}
					?>
						</div>
					<?php
				}
			}
		?>
	</div>
</div>
