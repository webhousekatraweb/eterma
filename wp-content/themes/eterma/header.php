<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta content="index, follow" name="robots">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class('animated fadeIn'); ?>>
<div class="se-pre-con"></div>
<!-- Header -->
<header class="banner-header" role="banner">
	<div class="top-header">
		<div class="container">
			<div class="row">
				<div class="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"/>
					</a>
				</div>

				<!-- Search form -->
				<form role="search" method="get" id="searchform" class="searchform"
				      action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<div class="search">
						<div class="search-input-cont">
							<input class="search-input" placeholder="Search..." name="s" id="s"> </input>
						</div>
						<span class="search-ico"></span>
					</div>
				</form>
				<!-- / Search form -->
			</div>
			<div class="nav-toggle"></div>
		</div>
	</div>
	<div class="container p-x-0">
		<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	</div>
</header>
<!-- /Header -->
<!-- Content -->
<div class="content">